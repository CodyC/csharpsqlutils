﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLUtilities
{
    public class WhereCondition
    {
        public StringBuilder SqlStringBuilder { get; set; }
        public SqlCommand SqlCmd { get; set; }
        public string FieldToCompare { get; set; }
        public string ValueToFind { get; set; }
        public string ComparisonOperator { get; set; }
        public bool IsFirstParameter { get; set; }

        public WhereCondition()
        {
            ComparisonOperator = "=";
        }
        
    }
}
