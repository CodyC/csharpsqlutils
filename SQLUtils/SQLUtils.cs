﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;

namespace SQLUtilities
{
    public static class SQLUtils
    {
        public const string OUTPUT_ID_CLAUSE = " OUTPUT INSERTED.ID ";
        public const string IS_NOT_OPERATOR = "is not";

        /// <summary>
        /// Appends a where condition parameter using the given stringbuilder and sqlCmd.
        /// Throws ArgumentException for null, empty or whitespace valueToFind.
        /// </summary>
        public static void AppendWhereConditionParameter(WhereCondition whereCondition)
        {
            AppendWhereConditionParameter(whereCondition.SqlStringBuilder,
                                          whereCondition.SqlCmd,
                                          whereCondition.FieldToCompare,
                                          whereCondition.ValueToFind,
                                          whereCondition.ComparisonOperator,
                                          whereCondition.IsFirstParameter);
        
        }

        /// <summary>
        /// Appends a where condition parameter using the given stringbuilder and sqlCmd.
        /// Throws ArgumentException for null, empty or whitespace valueToFind.
        /// </summary>
        public static void AppendWhereConditionParameter(StringBuilder sqlStringBuilder,
                                                         SqlCommand sqlCmd,
                                                         string fieldToCompare,
                                                         string valueToFind,
                                                         string comparisonOperator = "=",
                                                         bool isFirstParameter = false)
        {
            bool usingIsNotOperator = comparisonOperator.ToLower() == IS_NOT_OPERATOR.ToLower();

            if (!String.IsNullOrWhiteSpace(valueToFind)
                    || (valueToFind == null && usingIsNotOperator))
            {

                if (isFirstParameter)
                {
                    sqlStringBuilder.Append(" WHERE ");
                }
                else
                {
                    sqlStringBuilder.Append(" AND ");
                }

                sqlStringBuilder.Append(fieldToCompare);
                sqlStringBuilder.Append(" ");
                sqlStringBuilder.Append(comparisonOperator);
                sqlStringBuilder.Append(" ");

                if (usingIsNotOperator && (valueToFind == null || valueToFind.ToLower() == "null"))
                {
                    sqlStringBuilder.Append("null");
                }
                else
                {
                    string paramString = BuildParamName(fieldToCompare);

                    if (sqlCmd.Parameters.Contains(paramString))
                    {
                        paramString += "WhereParam";
                    }
                    sqlStringBuilder.Append(paramString);
                    sqlCmd.Parameters.AddWithValue(paramString, valueToFind);
                }
            }
            else
            {
                throw new ArgumentException(valueToFind);
            }
        }

        /// <summary>
        /// Appends a where condition parameter using the given stringbuilder and sqlCmd.
        /// Throws ArgumentException for null, empty or whitespace valueToFind.
        /// </summary>
        public static void AppendInsertParameter(StringBuilder sqlStringBuilder,
                                                 SqlCommand sqlCmd,
                                                 string fieldToFill,
                                                 object fillValue,
                                                 bool appendComma = true)
        {
            string paramString = BuildParamName(fieldToFill);

            sqlStringBuilder.Append(paramString);

            if (appendComma)
            {
                sqlStringBuilder.Append(',');
            }
               

            sqlCmd.Parameters.AddWithValue(paramString, fillValue ?? DBNull.Value);
        }

        /// <summary>
        /// Appends a where condition parameter using the given stringbuilder and sqlCmd.
        /// Throws ArgumentException for null, empty or whitespace valueToFind.
        /// </summary>
        public static void AppendUpdateParameter(StringBuilder sqlStringBuilder,
                                                 SqlCommand sqlCmd,
                                                 string fieldToFill,
                                                 object fillValue,
                                                 bool appendComma = true)
        {
            string paramString = BuildParamName(fieldToFill);

            sqlStringBuilder.Append( fieldToFill + "=" + paramString);

            if (appendComma)
            {
                sqlStringBuilder.Append(',');
            }

            sqlCmd.Parameters.AddWithValue(paramString, fillValue ?? DBNull.Value);
        }

        public static string BuildParamName(string columnName)
        {
            //clean up the column name a little bit to make a valid parameter
            //remove brackets, and replaces spaces and dots with underscores.
            return "@" + columnName
                            .Replace(' ', '_')
                            .Replace("[", "")
                            .Replace("]", "")
                            .Replace('.', '_');
        }
              
        public static void InsertDBRecord(string connectionString, string sqlInsertCommand)
        {
            using (SqlConnection sqlConn = new SqlConnection(connectionString))
            {
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.Connection = sqlConn;

                sqlCmd.CommandText = sqlInsertCommand;

                SQLUtils.PerformSQLInsert(sqlCmd);
            }
        }

        /// <summary>
        /// Performs assigned SQLCmd nonQuery, using sqlCmdText.  
        /// Returns rows affected.
        /// </summary>
        public static int PerformNonQueryCmd(SqlCommand sqlCmd)
        {
            int rowsAffected = 0;

            using (sqlCmd.Connection)
            {
                sqlCmd.Connection.Open();

                rowsAffected = sqlCmd.ExecuteNonQuery();

                sqlCmd.Connection.Close();
            }

            return rowsAffected;
        }


        /// <summary>
        /// performs assigned SQLCmd Insert, using sqlCmdText.  
        /// </summary>
        public static void PerformSQLInsert(SqlCommand sqlCmd)
        {
            using (sqlCmd.Connection)
            {
                sqlCmd.Connection.Open();
                sqlCmd.ExecuteScalar();
                sqlCmd.Connection.Close();
            }
        }

        public static void DeleteRecordByPrimaryKey(string tableName,
                                                    string primaryKeyName,
                                                    string primaryKeyValue,
                                                    string connString)
        {
            StringBuilder deleteStatementStringBuilder 
                = new StringBuilder("DELETE FROM " + tableName + " ");

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.Connection = new SqlConnection(connString);

            SQLUtils.AppendWhereConditionParameter(deleteStatementStringBuilder,
                                                   sqlCmd,
                                                   primaryKeyName,
                                                   primaryKeyValue,
                                                   isFirstParameter: true);

            sqlCmd.CommandText = deleteStatementStringBuilder.ToString();

            SQLUtils.PerformNonQueryCmd(sqlCmd);
        }

        public static bool ParseIntFromSQLReaderRow(SqlDataReader sqlReader, string columnName, out int parsedInt)
        {
            bool parseSuccess = false;

            int parsedValue = 0;

            string rowValueBuffer = sqlReader[columnName].ToString();

            parseSuccess = Int32.TryParse(rowValueBuffer, out parsedValue);

            if (parseSuccess)
            {
                parsedInt = parsedValue;
            }
            else
            {
                parsedInt = 0;
            }

            return parseSuccess;
        }
        
        public static bool ParseNullableIntFromSQLReaderRow(SqlDataReader sqlReader, string columnName, out int? parsedInt)
        {
            bool parseSuccess = false;

            int parsedValue = 0;

            string rowValueBuffer = sqlReader[columnName].ToString();

            if (String.IsNullOrWhiteSpace(rowValueBuffer))
            {
                parsedInt = null;
            }
            else
            {
                parseSuccess = Int32.TryParse(rowValueBuffer, out parsedValue);
            }

            if (parseSuccess)
            {
                parsedInt = parsedValue;
            }
            else
            {
                parsedInt = null;
            }

            return parseSuccess;
        }

        public static bool ParseNullableFloatFromSQLReaderRow(SqlDataReader sqlReader, string columnName, out float? parsedFloat)
        {
            bool parseSuccess = false;

            float parsedValue = 0.0F;

            string rowValueBuffer = sqlReader[columnName].ToString();

            if (String.IsNullOrWhiteSpace(rowValueBuffer))
            {
                parsedFloat = null;
            }
            else
            {
                parseSuccess = float.TryParse(rowValueBuffer, out parsedValue);
            }

            if (parseSuccess)
            {
                parsedFloat = parsedValue;
            }
            else
            {
                parsedFloat = null;
            }

            return parseSuccess;
        }

        public static T[] SQLCmdToGenericArray<T>(SqlCommand sqlCmd, string connectionString) where T : new() //T must have a default constructor
        {
            return SQLCmdToGenericList<T>(sqlCmd, connectionString).ToArray<T>();
        }

        public static List<T> SQLCmdToGenericList<T>(SqlCommand sqlCmd, string connectionString) where T : new() //T must have a default constructor
        {
            using (var sc = new SqlConnection(connectionString))
            {
                sqlCmd.Connection = sc;

                sqlCmd.Connection.Open();

                SqlDataReader sqldr;
                sqldr = sqlCmd.ExecuteReader();

                Type currentyType = typeof(T);
                PropertyInfo[] p = currentyType.GetProperties();
                
                List<PropertyInfo> propList = buildPropertyInfoList(sqldr, p);

                List<T> returnList = buildRecordsList<T>(sqldr, propList);

                sqlCmd.Cancel();
                sqldr.Close();

                return returnList;
            }
        }

        /// <summary>
        /// Returns a List<string> of the data stored in given columnName.
        /// </summary>
        /// <param name="sqlCmd"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static List<string> SQLCmdToList(SqlCommand sqlCmd, string columnName, string connectionString)
        {
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            sqlCmd.Connection = sqlConnection;

            SqlDataReader sqlDR = sqlCmd.ExecuteReader();

            var returnList = new List<string>();

            while (sqlDR.Read())
            {
                returnList.Add(sqlDR[columnName].ToString());
            }

            sqlConnection.Close();
            return returnList;
        }


        public static SqlCommand BuildInsertSQLCmd<T>(T inputData, string tableName, String connectionString)
        {

            SqlCommand sqlCmd = new SqlCommand();

            sqlCmd.Connection = new SqlConnection(connectionString);

            StringBuilder insertCommandSB = new StringBuilder(@"INSERT INTO " + tableName + @"(");

            //Look at a type and get the member names
            Type currentType = typeof(T);
            var memberPropertyInfoList = currentType.GetProperties().ToList();

            //Build the update statement
            int memberNameCount = memberPropertyInfoList.Count();
            int memberCounter = 1;

            foreach (var memberPropertyInfo in memberPropertyInfoList)
            {
                insertCommandSB.Append(memberPropertyInfo.Name);

                //is this the last record?
                bool isFinalMember = memberCounter == memberNameCount;

                if (!isFinalMember)
                {
                    //if not add a comma.
                    insertCommandSB.Append(", ");
                }

                memberCounter++;
            }

            //Close the Members clause
            insertCommandSB.Append(") "); 

            insertCommandSB.Append(" VALUES (");

            memberCounter = 1;
            foreach (var memberPropertyInfo in memberPropertyInfoList)
            {
                //is this the last record?
                bool isFinalMember = memberCounter == memberNameCount;

                //Build the parameters
                var currentProperty = currentType.GetProperty(memberPropertyInfo.Name);

                //get the name
                AppendInsertParameter(insertCommandSB,
                                        sqlCmd,
                                        memberPropertyInfo.Name,
                                        currentProperty.GetValue(inputData),
                                        !isFinalMember);

                memberCounter++;
                
            }

            //Close the values clause
            insertCommandSB.Append(" )  ");

            sqlCmd.CommandText = insertCommandSB.ToString();

            //Add the parameters to the sqlCmd
            return sqlCmd;
        }

        public static SqlCommand BuildUpdateSQLCmd<T>(T inputData, string tableName, String connectionString, string primaryKeyLiteral, string primaryKeyValue)
        {

            SqlCommand sqlCmd = new SqlCommand();

            sqlCmd.Connection = new SqlConnection(connectionString);

            string updateCmdPrefix = @"UPDATE " + tableName + " SET ";

            StringBuilder updateCommandSB = new StringBuilder(updateCmdPrefix);
            
            //Look at a type and get the member names
            Type currentType = typeof(T);
            var memberPropertyInfoList = currentType.GetProperties().ToList();


            //Build the update statement
            int memberNameCount = memberPropertyInfoList.Count();
            int memberCounter = 1;

            foreach (var memberPropertyInfo in memberPropertyInfoList)
            {
                //is this the last record?
                bool isFinalMember = memberCounter == memberNameCount;

                //Build the parameters
                var currentProperty = currentType.GetProperty(memberPropertyInfo.Name);

                
                Object fillValue = currentProperty.GetValue(inputData);

                AppendUpdateParameter(updateCommandSB, sqlCmd, memberPropertyInfo.Name, fillValue, !isFinalMember);               

                memberCounter++;
            }

            // Set the primary key of which record to update
            SQLUtils.AppendWhereConditionParameter(updateCommandSB, sqlCmd, primaryKeyLiteral, primaryKeyValue, isFirstParameter: true);
            
            sqlCmd.CommandText = updateCommandSB.ToString();

            //Add the parameters to the sqlCmd
            return sqlCmd;
        }


        private static List<T> buildRecordsList<T>(SqlDataReader sqlDR, List<PropertyInfo> propList) where T : new()
        {
            // Giddy up!
            object[] currentAttributes = null;
            List<T> returnList = new List<T>();
            while (sqlDR.Read())
            {
                T bufferObject = new T();
                foreach (PropertyInfo pi in propList)
                {
                    currentAttributes = pi.GetCustomAttributes(true);

                    if ((sqlDR[pi.Name] != System.DBNull.Value) && (currentAttributes.Length == 0))
                    {
                        object objectValueToSet = new object();

                        if (pi.PropertyType.IsEnum)
                        {
                            objectValueToSet = setEnumValue(sqlDR, pi, objectValueToSet);
                        }
                        else
                        {
                            Type typeToReflect
                                = Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType;

                            objectValueToSet = Convert.ChangeType(sqlDR[pi.Name], typeToReflect);
                        }

                        if (pi.CanWrite)
                        {
                            pi.SetValue(bufferObject, objectValueToSet, null);
                        }
                    }
                }

                returnList.Add(bufferObject);
            }

            return returnList;
        }

        private static object setEnumValue(SqlDataReader sqldr, PropertyInfo pi, object objectValueToSet)
        {
            string compareValue = "0";// use the default value of the enum if they don't supply us one

            string currentName = sqldr[pi.Name].ToString();

            if (!String.IsNullOrWhiteSpace(currentName))
            {
                compareValue = currentName;
            }

            objectValueToSet = Enum.Parse(pi.PropertyType, compareValue, true);
            return objectValueToSet;
        }

        private static List<PropertyInfo> buildPropertyInfoList(SqlDataReader sqldr, PropertyInfo[] p)
        {
            // Make a list and see which fields actually exist in the reader so I don't have to check over and over in the read loop

            List<PropertyInfo> propList = new List<PropertyInfo>();
            foreach (PropertyInfo pi in p)
            {
                for (int i = 0; i < sqldr.FieldCount; i++)
                {
                    if (sqldr.GetName(i).Equals(pi.Name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        propList.Add(pi);
                    }
                }
            }
            return propList;
        }

//        public static void GetSQLServerColumnDataType(string tableName, string columnName, string connectionString)
//        {
//            string getDataTypeSQL
//                = String.Format(@"SELECT DATA_TYPE 
//                                FROM INFORMATION_SCHEMA.COLUMNS
//                                WHERE 
//                                    TABLE_NAME = '{0}' 
//                                    AND 
//                                    COLUMN_NAME = '{1}'",
//                                    tableName,
//                                    columnName);

//            using (var sqlConn = new SqlConnection(connectionString))
//            {
//                sqlConn.Open();
//                var getDataTypeSqlCmd = new SqlCommand(getDataTypeSQL, sqlConn);

//                string columType = getDataTypeSqlCmd.ExecuteScalar().ToString();

//                //map the return value to the matching SqlDbType value
//                //return GetSqlDBType(columType);
//            }
//        }

        
        
    }
}
