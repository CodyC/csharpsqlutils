﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLUtilities;
//using SQLUtilities.WhereClausePair;

namespace SQLUtilities.PagedQuery
{
    public class PagedQuery
    {
        //public string TableName { get; set; }
        //public string OrderByField { get; set; }
        public int ResultsPerPage { get; set; }
        public int PageNumber { get; set; }
        //public string SelectClause { get; set; }
        //public string JoinClause { get; set; }
        //public string TableAlias { get; set; }
        //public List<WhereCondition> WhereConditionList { get; set; }

        private const string ROW_NUMBER_SELECT_CLAUSE_FORMAT_STRING = "ROW_NUMBER() OVER (ORDER BY {0}) AS RowNum";

        public const string PAGINATION_WHERE_CLAUSE = @"WHERE LT.RowNum BETWEEN ((@PageNumber-1)*@RowsPerPage)+1 AND @RowsPerPage*(@PageNumber)";


        public static string BuildRowNumberSelectClause(string orderByFieldName)
        {
            return String.Format(ROW_NUMBER_SELECT_CLAUSE_FORMAT_STRING, orderByFieldName);

        }

        public SqlCommand GetPagedResultsSqlCmd(SqlCommand sqlCommand)
        {
            SqlCommand paginatedSqlCommand = sqlCommand;

            string nestedSqlStatement = paginatedSqlCommand.CommandText;

            string pagedSqlStatement =
                String.Format(@"DECLARE @RowsPerPage INT = {0},
                                        @PageNumber INT = {1}
                                SELECT *
                                FROM 
	                                (
                                        {2}
                                    ) as LT
                                WHERE LT.RowNum BETWEEN ((@PageNumber-1)*@RowsPerPage)+1
                                AND @RowsPerPage*(@PageNumber)",
                                ResultsPerPage,
                                PageNumber,
                                nestedSqlStatement);


            //replace the commandText of the sqlCmd with the newly created one
            paginatedSqlCommand.CommandText = pagedSqlStatement;

            return paginatedSqlCommand;
        }

    }
}
