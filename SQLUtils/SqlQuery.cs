﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLUtilities
{
    public class SqlQuery
    {
        public StringBuilder SqlStringBuilder { get; set; }
        public SqlCommand SqlCmd { get; set; }
    }
}
